set tabstop=2
set shiftwidth=2
set smarttab
set et
set ai

set nu
set wrap

set pastetoggle=<F2>

